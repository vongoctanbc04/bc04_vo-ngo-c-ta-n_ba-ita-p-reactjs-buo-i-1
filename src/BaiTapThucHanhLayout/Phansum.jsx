import React, { Component } from 'react'
import Banner from './Banner';
import Body from './Body';
import Header from './Header';
import "./phancss.css";
import Phanitem from './Phanitem';

export default class Phansum extends Component {
  render() {
    return (
      <div className=''>
        <Header/>
        <Body/>
        <Banner/>
        <Phanitem/>
      </div>
    )
  }
}
